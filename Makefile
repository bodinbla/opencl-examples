SHELL := /bin/bash

$(info    Minimal samples                )
$(info ==================================)

all : build

build : 
	cd OPENCL && $(MAKE) $(MFLAGS)
	cd PAPI && $(MAKE) $(MFLAGS)
	cd MARE && $(MAKE) $(MFLAGS)
	cd MARE_1_0 && $(MAKE) $(MFLAGS)
	
test :
	cd OPENCL && $(MAKE) $(MFLAGS) test
	cd PAPI && $(MAKE) $(MFLAGS) test
	cd MARE && $(MAKE) $(MFLAGS) test
	cd MARE_1_0 && $(MAKE) $(MFLAGS)  test
	
clean :
	cd OPENCL && $(MAKE) $(MFLAGS) clean
	cd PAPI && $(MAKE) $(MFLAGS) clean
	cd MARE && $(MAKE) $(MFLAGS) clean
	cd MARE_1_0 && $(MAKE) $(MFLAGS)  clean

.PHONY : test build clean 

