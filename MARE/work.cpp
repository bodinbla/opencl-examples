
#include <mare/mare.h>
#include <mare/alignedallocator.hh>

//Create a page aligned allocator.
 template<class T, size_t Alignment>
 using aligned_vector = std::vector<T, mare::aligned_allocator<T, Alignment> >;
 template<class T>
 using page_aligned_vector = aligned_vector<T, 4096>;

struct uint2 { unsigned int x,y ; };

static uint  ratio           = 2;
static uint2 inputSize       = {640,480};
static uint2 computationSize = {inputSize.x / ratio , inputSize.y / ratio };

//Create a string containing OpenCL C kernel code.
#define OCL_KERNEL(name, k) static std::string const name##_string = #k

OCL_KERNEL(mm2metersKernel,
__kernel void mm2metersKernel(
		__global float * depth,
		const          uint2     depthSize ,
		const  __global ushort * in ,
		const          uint2     inSize  ,
		const   int ratio ){
	uint2 pixel = (uint2) (get_global_id(0),get_global_id(1));
    depth[pixel.x + depthSize.x * pixel.y] = in[pixel.x * ratio + inSize.x * pixel.y * ratio] / 1000.0f;
});

int
main(void)
{


    // pointer to a host location
    static page_aligned_vector<ushort> cpp_inputDepth_allocator(inputSize.x * inputSize.y);
    ushort * cpp_inputDepth = cpp_inputDepth_allocator.data();
    
    // declare a kernel
    static auto kernelmm2metersKernel = mare::create_gpu_kernel<
                                      mare::buffer_ptr<float>,
                                      uint2,
                                      mare::buffer_ptr<ushort>,
                                      uint2,
                                      int>(mm2metersKernel_string,"mm2metersKernel");

    // In and Out device buffer
    static auto ocl_inputDepth   = mare::create_buffer<ushort>(cpp_inputDepth,inputSize.x * inputSize.y);
    static auto ocl_floatDepth   = mare::create_buffer<float>(computationSize.x * computationSize.y);

    // using the data accesor, copy the host data to the device buffer (should work regarding the buffer definition)

    // create the GPU task
    int ratio = inputSize.x / computationSize.x;
    mare::range<2> kernel_0_globalworksize(computationSize.x,computationSize.y);
    auto kernel_0_GPU1  = mare::create_task(kernelmm2metersKernel,
						                    kernel_0_globalworksize,
						                    ocl_floatDepth,
						                    computationSize,
						                    ocl_inputDepth,
						                    inputSize,
						                    ratio
						    );





    // launch the GPU task
    kernel_0_GPU1->launch();
    // wait for it
    kernel_0_GPU1->wait_for();
    
    //sync the data
    ocl_floatDepth.sync();
    


}
