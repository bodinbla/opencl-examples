# MARE Examples #

These are minimal tests using the MARE (Multicore Asynchronous Runtime Environment).

## Compilation ##

To compile them, make sure to download MARE, and configure the CMakeLists.txt file :

````
set(MARE_DIR ~/Qualcomm/MARE/0.11.0/x86_64-linux-gnu/debug-gpu/lib/CMake/mare)
set(OPENCL_INCLUDE_DIR ~/OpenCL/include)
````

MARE_DIR is the CMake MARE utilities, and OPENCL_INCLUDE_DIR is valid OpenCL headers. 

