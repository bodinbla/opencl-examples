
#include <mare/mare.h>
#include <mare/alignedallocator.hh>

//Create a page aligned allocator.
template<class T, size_t Alignment>
using aligned_vector = std::vector<T, mare::aligned_allocator<T, Alignment> >;
template<class T>
using page_aligned_vector = aligned_vector<T, 4096>;

//Local type
struct uint2 { unsigned int x,y ; };

//Local parameters
static uint  ratio           = 2;
static uint2 inputSize       = {640,480};
static uint2 computationSize = {inputSize.x / ratio , inputSize.y / ratio };

//Create a string containing OpenCL C kernel code.
#define OCL_KERNEL(name, k) static std::string const name##_string = #k

//Declare the Kernel
OCL_KERNEL(mm2metersKernel,
__kernel void mm2metersKernel(
		__global float * depth,
		const          uint2     depthSize ,
		const  __global ushort * in ,
		const          uint2     inSize  ,
		const   int ratio ){
	uint2 pixel = (uint2) (get_global_id(0),get_global_id(1));
    depth[pixel.x + depthSize.x * pixel.y] = in[pixel.x * ratio + inSize.x * pixel.y * ratio] / 1000.0f;
});



// extern to func in order to be able to wait for the last task
mare::task_ptr<> kernels[2];
int frame = 0 ;

void waitthelast () {
  kernels[(frame) % 2]->wait_for();
}

void func (ushort * cpp_inputDepth) {
  

  frame += 1;


  std::cout << "Start func() with frame = " << frame  << std::endl;

  std::cout << "  - Create the kernel " << std::endl;

  // declare a static kernel
  static auto kernelmm2metersKernel = mare::create_gpu_kernel<
                                      mare::out<mare::buffer_ptr<float>>,
                                      uint2,
                                      mare::in<mare::buffer_ptr<ushort>>,
                                      uint2,
                                      int>(mm2metersKernel_string,"mm2metersKernel");


  std::cout << "  - Create the buffers " << std::endl;

  static page_aligned_vector<ushort> inputDepth_allocation(inputSize.x * inputSize.y);
  static auto ocl_inputDepth   = mare::create_buffer<ushort>(inputDepth_allocation.size());
  static auto ocl_floatDepth   = mare::create_buffer<float>(computationSize.x * computationSize.y);

  std::cout << "  - Copy data " << std::endl;


  memcpy((void*)&ocl_inputDepth[0],cpp_inputDepth,inputSize.x * inputSize.y * sizeof(ushort));

  std::cout << "  - Create Task " << std::endl;

  mare::range<2> kernel_0_globalworksize(computationSize.x,computationSize.y);

  auto kernel_0_GPU1  = mare::create_task(kernelmm2metersKernel,
					  kernel_0_globalworksize,
					  ocl_floatDepth,
					  computationSize,
					  ocl_inputDepth,
					  inputSize,
					  ratio);

  std::cout << "  - Set Task " << std::endl;

  kernels[frame % 2] = kernel_0_GPU1;

  
  if (frame > 1) {
    std::cout << "  - Wait previous Task " << std::endl;
    kernels[(frame + 1) % 2]->wait_for();
  }

  std::cout << "  - Launch Task " << std::endl;
  kernel_0_GPU1->launch();
  std::cout << "  - Done " << std::endl;


}


int
main(void)
{

    static page_aligned_vector<ushort> cpp_inputDepth(inputSize.x * inputSize.y);
    
    func(cpp_inputDepth.data());
    func(cpp_inputDepth.data());
    func(cpp_inputDepth.data());
    func(cpp_inputDepth.data());
    func(cpp_inputDepth.data());

    waitthelast();


}
