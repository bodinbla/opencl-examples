# PAPI samples #

This directory contains a sample of PAPI usage to count the number of instruction per cycle on a i5 or i7 CPU.

I'm not the author, I found it here http://stackoverflow.com/questions/8091182/how-to-read-performance-counters-on-i5-i7-cpus.
