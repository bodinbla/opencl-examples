
#include <mare/mare.h>
#include <mare/alignedallocator.hh>

//Create a page aligned allocator.
 template<class T, size_t Alignment>
 using aligned_vector = std::vector<T, mare::aligned_allocator<T, Alignment> >;
 template<class T>
 using page_aligned_vector = aligned_vector<T, 4096>;

struct uint2 { unsigned int x,y ; };

static uint  ratio           = 2;
static uint2 inputSize       = {640,480};
static uint2 computationSize = {inputSize.x / ratio , inputSize.y / ratio };

//Create a string containing OpenCL C kernel code.
#define OCL_KERNEL(name, k) static std::string const name##_string = #k

OCL_KERNEL(mm2metersKernel,
__kernel void mm2metersKernel(
		__global float * depth,
		const          uint2     depthSize ,
		const  __global ushort * in ,
		const          uint2     inSize  ,
		const   int ratio ){
	uint2 pixel = (uint2) (get_global_id(0),get_global_id(1));
    depth[pixel.x + depthSize.x * pixel.y] = in[pixel.x * ratio + inSize.x * pixel.y * ratio] / 1000.0f;
});




void func (ushort * cpp_inputDepth) {
  
  static int frame = 0 ;
  frame += 1;

  // declare a static kernel
  static auto kernelmm2metersKernel = mare::create_kernel<
                                      mare::buffer_ptr<float>,
                                      uint2,
                                      mare::buffer_ptr<ushort>,
                                      uint2,
                                      int>(mm2metersKernel_string,"mm2metersKernel");



  static page_aligned_vector<ushort> inputDepth_allocation(inputSize.x * inputSize.y);
  static auto ocl_inputDepth   = mare::create_buffer<ushort>(inputDepth_allocation.size());
  static auto ocl_floatDepth   = mare::create_buffer<float>(computationSize.x * computationSize.y);

  memcpy(ocl_inputDepth->data(),cpp_inputDepth,inputSize.x * inputSize.y * sizeof(ushort));

  static auto gpuattrsheader = mare::create_task_attrs(mare::attr::gpu);

  mare::range<2> kernel_0_globalworksize(computationSize.x,computationSize.y);

  auto kernel_0_GPU1  = mare::create_ndrange_task(
						kernel_0_globalworksize,
						mare::with_attrs(
								 gpuattrsheader,
								 kernelmm2metersKernel,
								 ocl_floatDepth,
								 computationSize,
								 ocl_inputDepth,
								 inputSize,
								 2
								 )
						    );




  static mare::task_ptr kernels[2];
  kernels[frame % 2] = kernel_0_GPU1;
  
  if (frame > 1) {
    mare::wait_for(kernels[(frame + 1) % 2]);
    kernels[(frame + 1) % 2].reset();
  }


  mare::launch(kernel_0_GPU1);


}


int
main(void)
{
    //Initialize the MARE runtime.
    mare::runtime::init();


    mare::runtime::init();

    static page_aligned_vector<ushort> cpp_inputDepth(inputSize.x * inputSize.y);

    
    func(cpp_inputDepth.data());

    // end of everything
    mare::runtime::shutdown();

}
