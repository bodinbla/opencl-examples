# MARE Examples #

These are minimal tests using the MARE (Multicore Asynchronous Runtime Environment).

## Examples ##

The available samples are : 

* Work  : A working example of MARE using OpenCL with the mm2meters kernel from SLAMBench (http://apt.cs.manchester.ac.uk/projects/PAMELA/tools/SLAMBench/).
* Test1, Test1bis,Test2 and Test3 : Same examples with some changes which work on nvidia (OpenCL drivers from CUDA 6.0  with a GT 640), but fail on intel (public OpenCL drivers for x86_64) or by using gdb. 


## Current execution results (using MARE 0.11) ##

Test name     | OpenCL 1.2 with Intel x86_64   | OpenCL 1.2 (patched) with Nvidia  |  
-------------:| :----------------------------- |:--------------------------------: |
Work          |   **OK** (**OK** with gdb)     |  **OK** (**OK** with gdb)         | 
Test1         |   **OK** (**NOK** with gdb)    |  **OK** (**NOK** with gdb)        | 
Test1bis      |   **OK** (**NOK** with gdb)    |  **OK** (**NOK** with gdb)        | 
Test2         |   **NOK** (**NOK** with gdb)   |  **OK** (**NOK** with gdb)        | 
Test3         |   **NOK** (**NOK** with gdb)   |  **OK** (**NOK** with gdb)        | 


## Compilation ##

To compile them, make sure to download MARE, and configure the CMakeLists.txt file :

````
set(MARE_DIR ~/Qualcomm/MARE/0.11.0/x86_64-linux-gnu/debug-gpu/lib/CMake/mare)
set(OPENCL_INCLUDE_DIR ~/OpenCL/include)
````

MARE_DIR is the CMake MARE utilities, and OPENCL_INCLUDE_DIR is valid OpenCL headers. 

