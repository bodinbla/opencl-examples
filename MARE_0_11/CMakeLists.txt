cmake_minimum_required(VERSION 2.8)

project(MARE_Test)

set(MARE_DIR ~/Qualcomm/MARE/0.11.0/x86_64-linux-gnu/debug-gpu/lib/CMake/mare)
set(OPENCL_INCLUDE_DIR ~/Qualcomm/MARE/0.11.0/include)

find_package(MARE   REQUIRED)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g -O3  --std=c++11 ")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -O3 --std=c++11 ${MARE_CXX_FLAGS}")

include_directories(${MARE_INCLUDE_DIRS})
include_directories(${OPENCL_INCLUDE_DIR})

  
add_executable(test1 test1.cpp)
target_link_libraries(test1 ${MARE_LIBRARIES})

add_executable(test1bis test1bis.cpp)
target_link_libraries(test1bis ${MARE_LIBRARIES})


add_executable(test2 test2.cpp)
target_link_libraries(test2 ${MARE_LIBRARIES})

add_executable(test3 test3.cpp)
target_link_libraries(test3 ${MARE_LIBRARIES})


add_executable(work work.cpp)
target_link_libraries(work ${MARE_LIBRARIES})