#ifdef __APPLE__
    #include <OpenCL/cl.h>
#else
    #include <CL/cl.h>
#endif
#include <numeric>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include "common.h"
#include <cmath>


#define DEFAULT_N 1000000
#define ITERATION     500


/*
 *
 *  In this example, only one context, one platform, and two devices.
 *
 *
 *
 *
 * */

cl_int clError = CL_SUCCESS;

cl_context context;
cl_program program;

cl_command_queue commandQueue;




const char * opencl_kernel_source() {

    const char *kernelSource =                                       "\n" \
    "__kernel void vecAdd(  __global float *a,                       \n" \
    "                       __global float *b,                       \n" \
    "                       __global float *c,                       \n" \
    "                       const unsigned int n)                    \n" \
    "{                                                               \n" \
    "    //Get our global thread ID                                  \n" \
    "    int id = get_global_id(0);                                  \n" \
    "                                                                \n" \
    "    //Make sure we do not go out of bounds                      \n" \
    "    if (id < n)                                                 \n" \
    "        c[id] = a[id] + b[id];                                  \n" \
    "}                                                               \n" \
                                                                    "\n" ;

    return kernelSource;


}

void do_statistics(std::string name, std::vector<double> v) {

    double sum = std::accumulate(v.begin(), v.end(), 0.0);
    double mean = sum / v.size();

    std::vector<double> diff(v.size());
    std::transform(v.begin(), v.end(), diff.begin(),
                   std::bind2nd(std::minus<double>(), mean));
    double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / v.size());

        std::cout << name << " : sum="  << sum
                << " mean=" << mean
                << " sq_sum=" << sq_sum
                << " stdev=" << stdev << std::endl;

}

void opencl_clean(void) {
    clReleaseContext(context);
    clReleaseCommandQueue(commandQueue);
    clReleaseProgram(program);
    return;
}

void opencl_init() {


    // get the platform
    // ************************************************************************

    cl_uint num_platforms;
    clError = clGetPlatformIDs(0, NULL, &num_platforms);
    checkErr(clError, "clGetPlatformIDs( 0, NULL, &num_platforms );");

    if (num_platforms <= 0) {
        std::cout << "No platform..." << std::endl;
        exit(1);
    }

    cl_platform_id* platforms = new cl_platform_id[num_platforms];
    clError = clGetPlatformIDs(num_platforms, platforms, NULL);
    checkErr(clError, "clGetPlatformIDs( num_platforms, &platforms, NULL );");

    if (num_platforms > 1) {
        char platformName[256];
        clError = clGetPlatformInfo(platforms[0], CL_PLATFORM_VENDOR,
                sizeof(platformName), platformName, NULL);
        std::cerr << "Multiple platforms found defaulting to: " << platformName
                << std::endl;
    }

    cl_platform_id  platform_id = platforms[0];

    delete platforms;

    // get the devices (and queues...)
    // ************************************************************************

    cl_uint device_count = 0;
    clError = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ALL, 0, NULL,
            &device_count);
    checkErr(clError, "Failed to create a device group");
    cl_device_id* deviceIds = (cl_device_id*) malloc(
            sizeof(cl_device_id) * device_count);
    clError = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ALL, device_count,
            deviceIds, NULL);



    char device_name[256];
    int compute_units;

    clError = clGetDeviceInfo(deviceIds[0 % device_count], CL_DEVICE_NAME,
            sizeof(device_name), device_name, NULL);
    checkErr(clError, "clGetDeviceInfo failed");
    clError = clGetDeviceInfo(deviceIds[0 % device_count], CL_DEVICE_MAX_COMPUTE_UNITS,
            sizeof(cl_uint), &compute_units, NULL);
    checkErr(clError, "clGetDeviceInfo failed");
    std::cerr << "Device is : " << device_name;
    std::cerr << " with " << compute_units << " compute units" << std::endl;

    cl_device_id device_id = deviceIds[0];


    // Create a compute context
    //
    context = clCreateContext(0, device_count, deviceIds, NULL, NULL, &clError);
    checkErr(clError, "Failed to create a compute context!");

    delete deviceIds;

    // Create a command commands
    //
    commandQueue = clCreateCommandQueue(context, device_id, 0, &clError);
    checkErr(clError, "Failed to create a command commands!");

    const char* charSource = opencl_kernel_source();

    // Create the compute program from the source buffer
    //
    program = clCreateProgramWithSource(context, 1, (const char **) &charSource,
            NULL, &clError);
    if (!program) {
        std::cout << "Error: Failed to create compute program!" << std::endl;
        exit(1);
    }

    // Build the program executable
    //
    clError = clBuildProgram(program, 0, NULL, NULL, NULL,
            NULL);

    /* Get the size of the build log. */
    size_t logSize = 0;
    clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL,
            &logSize);

    if (clError != CL_SUCCESS) {
        if (logSize > 1) {
            char* log = new char[logSize];
            clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG,
                    logSize, log, NULL);

            std::string stringChars(log, logSize);
            std::cerr << "Build log:\n " << stringChars << std::endl;

            delete[] log;
        }
        std::cout <<  "Error: Failed to build program executable!" << std::endl;
        exit(1);
    }

    return;

}


int main(int argc, char ** argv) {


    std::vector<double> duration_initialization_map;
    std::vector<double> duration_initialization_mapunmap;
    std::vector<double> duration_initialization_std;
    std::vector<double> duration_computation_map;
    std::vector<double> duration_computation_mapunmap;
    std::vector<double> duration_computation_std;

  size_t N = DEFAULT_N;

  std::cout << "This is a minimal OpenCL example (float vecAdd) using two devices at the same time. Parameter is N the size of the vector." << std::endl;
  if (argc >= 2) {
     N = atoi(argv[1]);
  }

  std::cout << "N = " << N << std::endl;

  std::cout << "Initialize OpenCL." << std::endl;
  opencl_init();

  std::cout << "Initialize Buffers A,B and C." << std::endl;

  // Allocate memory for each vector on host
  float * h_a = (float*)malloc(N * sizeof(float));
  float * h_b = (float*)malloc(N * sizeof(float));
  float * h_c = (float*)malloc(N * sizeof(float));


  cl_mem d_a  = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR  , N * sizeof(float) , NULL , &clError);
  cl_mem d_b  = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR  , N * sizeof(float) , NULL , &clError);
  cl_mem d_c  = clCreateBuffer(context,  CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR  , N * sizeof(float) , NULL , &clError);

  // Allocate memory for each vector on host
  float * m_a = (float*)clEnqueueMapBuffer(commandQueue, d_a, CL_TRUE, CL_MAP_WRITE, 0, N * sizeof(float), 0, NULL, NULL, &clError);
  float * m_b = (float*)clEnqueueMapBuffer(commandQueue, d_b, CL_TRUE, CL_MAP_WRITE, 0, N * sizeof(float), 0, NULL, NULL, &clError);
  float * m_c = (float*)clEnqueueMapBuffer(commandQueue, d_c, CL_TRUE, CL_MAP_WRITE, 0, N * sizeof(float), 0, NULL, NULL, &clError);


  // Initialize vectors on host

  std::cout << "Writing test (initialization)." << std::endl;


  std::cout << " - using malloc." << std::endl;

  tock();

  for (unsigned int j = 0 ; j < ITERATION ; j++) {


      for(  unsigned int  i = 0; i < N; i++ )
      {
          h_a[i] = sinf(i)*sinf(i);
          h_b[i] = cosf(i)*cosf(i);
          h_c[i] = 0;
      }

      duration_initialization_std.push_back(tock());

  }

  std::cout << " - using clEnqueueMapBuffer." << std::endl;

  tock();

  for (unsigned int j = 0 ; j < ITERATION ; j++) {


      for(  unsigned int  i = 0; i < N; i++ )
      {
          m_a[i] = sinf(i)*sinf(i);
          m_b[i] = cosf(i)*cosf(i);
          m_c[i] = 0;
      }

      duration_initialization_map.push_back(tock());

  }


  std::cout << " - using clEnqueueMapBuffer (including map unmap)." << std::endl;

  tock();

  clEnqueueUnmapMemObject(commandQueue, d_a, m_a, 0, NULL, NULL);
  clEnqueueUnmapMemObject(commandQueue, d_b, m_b, 0, NULL, NULL);
  clEnqueueUnmapMemObject(commandQueue, d_c, m_c, 0, NULL, NULL);

  for (unsigned int j = 0 ; j < ITERATION ; j++) {

      // Allocate memory for each vector on host
      m_a = (float*)clEnqueueMapBuffer(commandQueue, d_a, CL_TRUE, CL_MAP_WRITE, 0, N * sizeof(float), 0, NULL, NULL, &clError);
      m_b = (float*)clEnqueueMapBuffer(commandQueue, d_b, CL_TRUE, CL_MAP_WRITE, 0, N * sizeof(float), 0, NULL, NULL, &clError);
      m_c = (float*)clEnqueueMapBuffer(commandQueue, d_c, CL_TRUE, CL_MAP_WRITE, 0, N * sizeof(float), 0, NULL, NULL, &clError);


      for(  unsigned int  i = 0; i < N; i++ )
      {
          m_a[i] = sinf(i)*sinf(i);
          m_b[i] = cosf(i)*cosf(i);
          m_c[i] = 0;
      }

      clEnqueueUnmapMemObject(commandQueue, d_a, m_a, 0, NULL, NULL);
      clEnqueueUnmapMemObject(commandQueue, d_b, m_b, 0, NULL, NULL);
      clEnqueueUnmapMemObject(commandQueue, d_c, m_c, 0, NULL, NULL);

      duration_initialization_mapunmap.push_back(tock());

  }


  std::cout << "Reading + Writing test (computation)." << std::endl;



  std::cout << " - using malloc." << std::endl;

  tock();

  for (unsigned int j = 0 ; j < ITERATION ; j++) {


      for(  unsigned int  i = 0; i < N; i++ )
      {
          h_c[i] = h_a[i] + h_b[i];
      }

      duration_computation_std.push_back(tock());

  }

  std::cout << " - using clEnqueueMapBuffer." << std::endl;

  tock();

  for (unsigned int j = 0 ; j < ITERATION ; j++) {


      for(  unsigned int  i = 0; i < N; i++ )
      {

          m_c[i] = m_a[i] + m_b[i];
      }

      duration_computation_map.push_back(tock());

  }


  std::cout << " - using clEnqueueMapBuffer (including map unmap)." << std::endl;

  tock();

  clEnqueueUnmapMemObject(commandQueue, d_a, m_a, 0, NULL, NULL);
  clEnqueueUnmapMemObject(commandQueue, d_b, m_b, 0, NULL, NULL);
  clEnqueueUnmapMemObject(commandQueue, d_c, m_c, 0, NULL, NULL);

  for (unsigned int j = 0 ; j < ITERATION ; j++) {

      // Allocate memory for each vector on host
      m_a = (float*)clEnqueueMapBuffer(commandQueue, d_a, CL_TRUE, CL_MAP_WRITE, 0, N * sizeof(float), 0, NULL, NULL, &clError);
      m_b = (float*)clEnqueueMapBuffer(commandQueue, d_b, CL_TRUE, CL_MAP_WRITE, 0, N * sizeof(float), 0, NULL, NULL, &clError);
      m_c = (float*)clEnqueueMapBuffer(commandQueue, d_c, CL_TRUE, CL_MAP_WRITE, 0, N * sizeof(float), 0, NULL, NULL, &clError);


      for(  unsigned int  i = 0; i < N; i++ )
      {
          m_c[i] = m_a[i] + m_b[i];
      }

      clEnqueueUnmapMemObject(commandQueue, d_a, m_a, 0, NULL, NULL);
      clEnqueueUnmapMemObject(commandQueue, d_b, m_b, 0, NULL, NULL);
      clEnqueueUnmapMemObject(commandQueue, d_c, m_c, 0, NULL, NULL);

      duration_computation_mapunmap.push_back(tock());

  }



  do_statistics ("Host Write (standard)", duration_initialization_std);
  do_statistics ("Host Write (clEnqueueMapBuffer)", duration_initialization_map);
  do_statistics ("Host Write (clEnqueueMapBuffer+unMap)", duration_initialization_mapunmap);

  do_statistics ("Host Read (standard)", duration_computation_std);
  do_statistics ("Host Read (clEnqueueMapBuffer)", duration_computation_map);
  do_statistics ("Host Read (clEnqueueMapBuffer+unMap)", duration_computation_mapunmap);


  std::cout << "Clean OpenCL." << std::endl;
  opencl_clean();


  std::cout << "End of the program." << std::endl;


}
