# OpenCL Examples #

This directory contains a few openCL examples of my own.

## Shared memory ##

Run on Intel OpenCL 

```
This is a minimal OpenCL example (float vecAdd) using two devices at the same time. Parameter is N the size of the vector.
N = 1000000
Initialize OpenCL.
Device is : Intel(R) Core(TM) i5-4570 CPU @ 3.20GHz with 4 compute units
Initialize Buffers A,B and C.
Writing test (initialization).
 - using malloc.
 - using clEnqueueMapBuffer.
 - using clEnqueueMapBuffer (including map unmap).
Reading + Writing test (computation).
 - using malloc.
 - using clEnqueueMapBuffer.
 - using clEnqueueMapBuffer (including map unmap).
Host Write (standard) : sum=13.4563 mean=0.0269126 sq_sum=0.000223471 stdev=0.000668537
Host Write (clEnqueueMapBuffer) : sum=13.2431 mean=0.0264861 sq_sum=2.35724e-05 stdev=0.000217128
Host Write (clEnqueueMapBuffer+unMap) : sum=13.7109 mean=0.0274219 sq_sum=0.000297193 stdev=0.000770964
Host Read (standard) : sum=0.441949 mean=0.000883898 sq_sum=5.13253e-06 stdev=0.000101317
Host Read (clEnqueueMapBuffer) : sum=0.49695 mean=0.000993901 sq_sum=7.25843e-06 stdev=0.000120486
Host Read (clEnqueueMapBuffer+unMap) : sum=0.555286 mean=0.00111057 sq_sum=1.55145e-05 stdev=0.000176151
Clean OpenCL.
End of the program.
```

Run on MALI  (by dividing the number N by 100)


```
This is a minimal OpenCL example (float vecAdd) using two devices at the same time. Parameter is N the size of the vector.
N = 10000
Initialize OpenCL.
Device is : Mali-T628 with 4 compute units
Initialize Buffers A,B and C.
Writing test (initialization).
 - using malloc.
 - using clEnqueueMapBuffer.
 - using clEnqueueMapBuffer (including map unmap).
Reading + Writing test (computation).
 - using malloc.
 - using clEnqueueMapBuffer.
 - using clEnqueueMapBuffer (including map unmap).
Host Write (standard) : sum=4.57625 mean=0.00915249 sq_sum=1.35489e-06 stdev=5.20555e-05
Host Write (clEnqueueMapBuffer) : sum=4.58163 mean=0.00916326 sq_sum=2.257e-06 stdev=6.71864e-05
Host Write (clEnqueueMapBuffer+unMap) : sum=4.92482 mean=0.00984964 sq_sum=1.08939e-06 stdev=4.66774e-05
Host Read (standard) : sum=0.0106598 mean=2.13196e-05 sq_sum=2.98577e-09 stdev=2.44367e-06
Host Read (clEnqueueMapBuffer) : sum=0.0106375 mean=2.1275e-05 sq_sum=2.55055e-09 stdev=2.25856e-06
Host Read (clEnqueueMapBuffer+unMap) : sum=0.372054 mean=0.000744109 sq_sum=9.94164e-08 stdev=1.41008e-05
Clean OpenCL.
End of the program.
```


run on Nvidia GT640
```
./examples/shared_memory: /usr/lib64/nvidia/libOpenCL.so.1: no version information available (required by ./examples/shared_memory)
This is a minimal OpenCL example (float vecAdd) using two devices at the same time. Parameter is N the size of the vector.
N = 1000000
Initialize OpenCL.
Multiple platforms found defaulting to: NVIDIA Corporation
Device is : GeForce GT 640 with 2 compute units
Initialize Buffers A,B and C.
Writing test (initialization).
 - using malloc.
 - using clEnqueueMapBuffer.
 - using clEnqueueMapBuffer (including map unmap).
Reading + Writing test (computation).
 - using malloc.
 - using clEnqueueMapBuffer.
 - using clEnqueueMapBuffer (including map unmap).
Host Write (standard) : sum=13.1022 mean=0.0262043 sq_sum=7.14677e-06 stdev=0.000119556
Host Write (clEnqueueMapBuffer) : sum=13.2202 mean=0.0264404 sq_sum=0.000111893 stdev=0.00047306
Host Write (clEnqueueMapBuffer+unMap) : sum=14.5224 mean=0.0290448 sq_sum=0.0002312 stdev=0.000679999
Host Read (standard) : sum=0.517568 mean=0.00103514 sq_sum=7.7461e-05 stdev=0.000393601
Host Read (clEnqueueMapBuffer) : sum=0.444587 mean=0.000889175 sq_sum=2.7703e-06 stdev=7.44353e-05
Host Read (clEnqueueMapBuffer+unMap) : sum=1.42145 mean=0.00284291 sq_sum=4.3848e-06 stdev=9.36461e-05
Clean OpenCL.
End of the program.
```


## Dual device ##

On MALI 
```
root@odroid:~/opencl-minimal# ./examples/two_devices 
This is a minimal OpenCL example (float vecAdd) using two devices at the same time. Parameter is N the size of the vector.
N = 10000000
Initialize OpenCL.
First device is : Mali-T628 with 4 compute units
Second device is : Mali-T628 with 2 compute units
Initialize Buffers A,B and C.
Initialize kernel vecAdd.
Set kernel arguments.
**** EXECUTE THE KERNEL ON commandQueue1 *****
final result: 1
**** EXECUTE THE KERNEL ON commandQueue2 *****
final result: 1
**** EXECUTE THE KERNEL ON commandQueue1 and commandQueue2 (partition 50/50) *****
final result: 1
**** EXECUTE THE KERNEL ON commandQueue1 and commandQueue2 (partition 20/80) *****
final result: 1
**** EXECUTE THE KERNEL ON commandQueue1 and commandQueue2 (partition 80/20) *****
final result: 1
Do final statistics.
device 1 : sum=25.2317 mean=0.0252317 sq_sum=0.00117323 stdev=0.00108316
device 2 : sum=42.0947 mean=0.0420947 sq_sum=6.65654e-06 stdev=8.15876e-05
device 1+2 (50/50) : sum=37.9749 mean=0.0379749 sq_sum=4.15545e-05 stdev=0.000203849
device 1+2 (20/80) : sum=45.5876 mean=0.0455876 sq_sum=2.29779e-06 stdev=4.79353e-05
device 1+2 (80/20) : sum=30.4611 mean=0.0304611 sq_sum=3.52209e-06 stdev=5.93472e-05
Clean OpenCL.
End of the program.
```

# Licence # 

This sample is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This sample is  is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this sample.  If not, see <http://www.gnu.org/licenses/>.
