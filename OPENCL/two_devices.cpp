
#include <CL/cl.h>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include "common.h"
#include <cmath>


#define DEFAULT_N 10000000
#define ITERATION     1000


/*
 *
 *  In this example, only one context, one platform, and two devices.
 *
 *
 *
 *
 * */

cl_int clError = CL_SUCCESS;

cl_context context;
cl_program program;

cl_command_queue commandQueue1;
cl_command_queue commandQueue2;

const char * opencl_kernel_source() {

    const char *kernelSource =                                       "\n" \
    "__kernel void vecAdd(  __global float *a,                       \n" \
    "                       __global float *b,                       \n" \
    "                       __global float *c,                       \n" \
    "                       const unsigned int n)                    \n" \
    "{                                                               \n" \
    "    //Get our global thread ID                                  \n" \
    "    int id = get_global_id(0);                                  \n" \
    "                                                                \n" \
    "    //Make sure we do not go out of bounds                      \n" \
    "    if (id < n)                                                 \n" \
    "        c[id] = a[id] + b[id];                                  \n" \
    "}                                                               \n" \
                                                                    "\n" ;

    return kernelSource;


}

void do_statistics(std::string name, std::vector<double> v) {

    double sum = std::accumulate(v.begin(), v.end(), 0.0);
    double mean = sum / v.size();

    std::vector<double> diff(v.size());
    std::transform(v.begin(), v.end(), diff.begin(),
                   std::bind2nd(std::minus<double>(), mean));
    double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    double stdev = std::sqrt(sq_sum / v.size());

        std::cout << name << " : sum="  << sum
                << " mean=" << mean
                << " sq_sum=" << sq_sum
                << " stdev=" << stdev << std::endl;

}

void opencl_clean(void) {
    clReleaseContext(context);
    clReleaseCommandQueue(commandQueue1);
    clReleaseCommandQueue(commandQueue2);
    clReleaseProgram(program);
    return;
}

void opencl_init() {


    // get the platform
    // ************************************************************************

    cl_uint num_platforms;
    clError = clGetPlatformIDs(0, NULL, &num_platforms);
    checkErr(clError, "clGetPlatformIDs( 0, NULL, &num_platforms );");

    if (num_platforms <= 0) {
        std::cout << "No platform..." << std::endl;
        exit(1);
    }

    cl_platform_id* platforms = new cl_platform_id[num_platforms];
    clError = clGetPlatformIDs(num_platforms, platforms, NULL);
    checkErr(clError, "clGetPlatformIDs( num_platforms, &platforms, NULL );");

    if (num_platforms > 1) {
        char platformName[256];
        clError = clGetPlatformInfo(platforms[0], CL_PLATFORM_VENDOR,
                sizeof(platformName), platformName, NULL);
        std::cerr << "Multiple platforms found defaulting to: " << platformName
                << std::endl;
    }

    cl_platform_id  platform_id = platforms[0];

    delete platforms;

    // get the devices (and queues...)
    // ************************************************************************

    cl_uint device_count = 0;
    clError = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ALL, 0, NULL,
            &device_count);
    checkErr(clError, "Failed to create a device group");
    cl_device_id* deviceIds = (cl_device_id*) malloc(
            sizeof(cl_device_id) * device_count);
    clError = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ALL, device_count,
            deviceIds, NULL);

    if (device_count < 2) {
        std::cout << "[WARNING] This example needs at least two devices on the main platform..." << std::endl;
    }

    char device_name[256];
    int compute_units;

    clError = clGetDeviceInfo(deviceIds[0 % device_count], CL_DEVICE_NAME,
            sizeof(device_name), device_name, NULL);
    checkErr(clError, "clGetDeviceInfo failed");
    clError = clGetDeviceInfo(deviceIds[0 % device_count], CL_DEVICE_MAX_COMPUTE_UNITS,
            sizeof(cl_uint), &compute_units, NULL);
    checkErr(clError, "clGetDeviceInfo failed");
    std::cerr << "First device is : " << device_name;
    std::cerr << " with " << compute_units << " compute units" << std::endl;

    clError = clGetDeviceInfo(deviceIds[1 % device_count], CL_DEVICE_NAME,
            sizeof(device_name), device_name, NULL);
    checkErr(clError, "clGetDeviceInfo failed");
    clError = clGetDeviceInfo(deviceIds[1 % device_count], CL_DEVICE_MAX_COMPUTE_UNITS,
            sizeof(cl_uint), &compute_units, NULL);
    checkErr(clError, "clGetDeviceInfo failed");
    std::cerr << "Second device is : " << device_name;
    std::cerr << " with " << compute_units << " compute units" << std::endl;

    cl_device_id device_id1 = deviceIds[0 % device_count];
    cl_device_id device_id2 = deviceIds[1 % device_count];


    // Create a compute context
    //
    context = clCreateContext(0, device_count, deviceIds, NULL, NULL, &clError);
    checkErr(clError, "Failed to create a compute context!");

    delete deviceIds;

    // Create a command commands
    //
    commandQueue1 = clCreateCommandQueue(context, device_id1, 0, &clError);
    commandQueue2 = clCreateCommandQueue(context, device_id2, 0, &clError);
    checkErr(clError, "Failed to create a command commands!");

    const char* charSource = opencl_kernel_source();

    // Create the compute program from the source buffer
    //
    program = clCreateProgramWithSource(context, 1, (const char **) &charSource,
            NULL, &clError);
    if (!program) {
        std::cout << "Error: Failed to create compute program!" << std::endl;
        exit(1);
    }

    // Build the program executable
    //
    clError = clBuildProgram(program, 0, NULL, NULL, NULL,
            NULL);

    /* Get the size of the build log. */
    size_t logSize = 0;
    clGetProgramBuildInfo(program, device_id1, CL_PROGRAM_BUILD_LOG, 0, NULL,
            &logSize);

    if (clError != CL_SUCCESS) {
        if (logSize > 1) {
            char* log = new char[logSize];
            clGetProgramBuildInfo(program, device_id1, CL_PROGRAM_BUILD_LOG,
                    logSize, log, NULL);

            std::string stringChars(log, logSize);
            std::cerr << "Build log:\n " << stringChars << std::endl;

            delete[] log;
        }
        std::cout <<  "Error: Failed to build program executable!" << std::endl;
        exit(1);
    }

    return;

}


int main(int argc, char ** argv) {


    std::vector<double> duration_100_000;
    std::vector<double> duration_080_020;
    std::vector<double> duration_050_050;
    std::vector<double> duration_020_080;
    std::vector<double> duration_000_100;


  size_t N = DEFAULT_N;

  std::cout << "This is a minimal OpenCL example (float vecAdd) using two devices at the same time. Parameter is N the size of the vector." << std::endl;
  if (argc >= 2) {
     N = atoi(argv[1]);
  }

  std::cout << "N = " << N << std::endl;

  std::cout << "Initialize OpenCL." << std::endl;
  opencl_init();

  std::cout << "Initialize Buffers A,B and C." << std::endl;

  // Allocate memory for each vector on host
  float * h_a = (float*)malloc(N * sizeof(float));
  float * h_b = (float*)malloc(N * sizeof(float));
  float * h_c = (float*)malloc(N * sizeof(float));

  // Initialize vectors on host

  for(  unsigned int  i = 0; i < N; i++ )
  {
      h_a[i] = sinf(i)*sinf(i);
      h_b[i] = cosf(i)*cosf(i);
      h_c[i] = 0;
  }


  cl_mem d_a  = clCreateBuffer(context,  CL_MEM_READ_WRITE  , N * sizeof(float) , NULL , &clError);
  cl_mem d_b  = clCreateBuffer(context,  CL_MEM_READ_WRITE  , N * sizeof(float) , NULL , &clError);
  cl_mem d_c  = clCreateBuffer(context,  CL_MEM_READ_WRITE  , N * sizeof(float) , NULL , &clError);

  // Write our data set into the input array in device memory
  clError  = clEnqueueWriteBuffer(commandQueue1, d_a, CL_TRUE, 0,   N * sizeof(float) , h_a, 0, NULL, NULL);
  clError |= clEnqueueWriteBuffer(commandQueue1, d_b, CL_TRUE, 0,   N * sizeof(float) , h_b, 0, NULL, NULL);


  checkErr(clError, "Failed to copy the data!");

  std::cout << "Initialize kernel vecAdd." << std::endl;

  static cl_kernel vecAdd_kernel = clCreateKernel(program, "vecAdd", &clError);

  std::cout << "Set kernel arguments." << std::endl;

  // Set the arguments to our compute kernel
  clError  = clSetKernelArg(vecAdd_kernel, 0, sizeof(cl_mem), &d_a);
  clError |= clSetKernelArg(vecAdd_kernel, 1, sizeof(cl_mem), &d_b);
  clError |= clSetKernelArg(vecAdd_kernel, 2, sizeof(cl_mem), &d_c);
  clError |= clSetKernelArg(vecAdd_kernel, 3, sizeof(unsigned int), &N);
  checkErr(clError, "Failed to set the arguments!");






  {

  std::cout << "**** EXECUTE THE KERNEL ON commandQueue1 *****" << std::endl;

  tock();


  for (unsigned int j = 0 ; j < ITERATION ; j++) {

      // Execute the kernel over the entire range of the data set
      clError = clEnqueueNDRangeKernel(commandQueue1, vecAdd_kernel, 1, NULL, &N, NULL, 0, NULL, NULL);
      checkErr(clError, "Failed to execute the kernel!");

      // Wait for the command queue to get serviced before reading back results
      clFinish(commandQueue1);

      duration_100_000.push_back(tock());

  }



  // Read the results from the device
  clEnqueueReadBuffer(commandQueue1, d_c, CL_TRUE, 0, N * sizeof(float), h_c, 0, NULL, NULL );


  double sum = 0;
  for(   unsigned int  i = 0; i < N; i++ )
      sum += h_c[i];
  std::cout << "final result: " << sum/N << std::endl;


}

  {

  std::cout << "**** EXECUTE THE KERNEL ON commandQueue2 *****" << std::endl;

  tock();


  for (unsigned int j = 0 ; j < ITERATION ; j++) {

      // Execute the kernel over the entire range of the data set
      clError = clEnqueueNDRangeKernel(commandQueue2, vecAdd_kernel, 1, NULL, &N, NULL, 0, NULL, NULL);
      checkErr(clError, "Failed to execute the kernel!");

      // Wait for the command queue to get serviced before reading back results
      clFinish(commandQueue2);

      duration_000_100.push_back(tock());

  }




  // Read the results from the device
  clEnqueueReadBuffer(commandQueue2, d_c, CL_TRUE, 0, N * sizeof(float), h_c, 0, NULL, NULL );


  double sum = 0;
  for(   unsigned int  i = 0; i < N; i++ )
      sum += h_c[i];
  std::cout << "final result: " << sum/N << std::endl;


  }




  {

  std::cout << "**** EXECUTE THE KERNEL ON commandQueue1 and commandQueue2 (partition 50/50) *****" << std::endl;

  tock();


  for (unsigned int j = 0 ; j < ITERATION ; j++) {

      size_t global_work_offset = N/2;

      // Execute the kernel over the entire range of the data set
      clError = clEnqueueNDRangeKernel(commandQueue1, vecAdd_kernel, 1, NULL,               &global_work_offset, NULL, 0, NULL, NULL);
      checkErr(clError, "Failed to execute the kernel!");

      // Execute the kernel over the entire range of the data set
      clError = clEnqueueNDRangeKernel(commandQueue2, vecAdd_kernel, 1,  &global_work_offset, &global_work_offset, NULL, 0, NULL, NULL);
      checkErr(clError, "Failed to execute the kernel!");

      // Wait for the command queue to get serviced before reading back results
      clFinish(commandQueue1);
      clFinish(commandQueue2);

      duration_050_050.push_back(tock());

  }


  // Read the results from the device
  clEnqueueReadBuffer(commandQueue2, d_c, CL_TRUE, 0, N * sizeof(float), h_c, 0, NULL, NULL );


  double sum = 0;
  for(   unsigned int  i = 0; i < N; i++ )
      sum += h_c[i];
  std::cout << "final result: " << sum/N << std::endl;


  }



  {

  std::cout << "**** EXECUTE THE KERNEL ON commandQueue1 and commandQueue2 (partition 20/80) *****" << std::endl;

  tock();


  for (unsigned int j = 0 ; j < ITERATION ; j++) {

      size_t global_work_offset = N/5;
      size_t global_work_size_remained   =N -  global_work_offset;

      // Execute the kernel over the entire range of the data set
      clError = clEnqueueNDRangeKernel(commandQueue1, vecAdd_kernel, 1, NULL,               &global_work_offset, NULL, 0, NULL, NULL);
      checkErr(clError, "Failed to execute the kernel!");

      // Execute the kernel over the entire range of the data set
      clError = clEnqueueNDRangeKernel(commandQueue2, vecAdd_kernel, 1,  &global_work_offset, &global_work_size_remained, NULL, 0, NULL, NULL);
      checkErr(clError, "Failed to execute the kernel!");

      // Wait for the command queue to get serviced before reading back results
      clFinish(commandQueue1);
      clFinish(commandQueue2);

      duration_020_080.push_back(tock());

  }


  // Read the results from the device
  clEnqueueReadBuffer(commandQueue2, d_c, CL_TRUE, 0, N * sizeof(float), h_c, 0, NULL, NULL );


  double sum = 0;
  for(   unsigned int  i = 0; i < N; i++ )
      sum += h_c[i];
  std::cout << "final result: " << sum/N << std::endl;


  }



  {

  std::cout << "**** EXECUTE THE KERNEL ON commandQueue1 and commandQueue2 (partition 80/20) *****" << std::endl;

  tock();


  for (unsigned int j = 0 ; j < ITERATION ; j++) {

      size_t global_work_offset = 4 * (N/5);
      size_t global_work_size_remained   =N -  global_work_offset;

      // Execute the kernel over the entire range of the data set
      clError = clEnqueueNDRangeKernel(commandQueue1, vecAdd_kernel, 1, NULL,               &global_work_offset, NULL, 0, NULL, NULL);
      checkErr(clError, "Failed to execute the kernel!");

      // Execute the kernel over the entire range of the data set
      clError = clEnqueueNDRangeKernel(commandQueue2, vecAdd_kernel, 1,  &global_work_offset, &global_work_size_remained, NULL, 0, NULL, NULL);
      checkErr(clError, "Failed to execute the kernel!");

      // Wait for the command queue to get serviced before reading back results
      clFinish(commandQueue1);
      clFinish(commandQueue2);

      duration_080_020.push_back(tock());

  }


  // Read the results from the device
  clEnqueueReadBuffer(commandQueue2, d_c, CL_TRUE, 0, N * sizeof(float), h_c, 0, NULL, NULL );


  double sum = 0;
  for(   unsigned int  i = 0; i < N; i++ )
      sum += h_c[i];
  std::cout << "final result: " << sum/N << std::endl;


  }


  std::cout << "Do final statistics." << std::endl;




  do_statistics ("device 1", duration_100_000);
  do_statistics ("device 2", duration_000_100);
  do_statistics ("device 1+2 (50/50)", duration_050_050);
  do_statistics ("device 1+2 (20/80)", duration_020_080);
  do_statistics ("device 1+2 (80/20)", duration_080_020);


  std::cout << "Clean OpenCL." << std::endl;
  opencl_clean();


  std::cout << "End of the program." << std::endl;


}
