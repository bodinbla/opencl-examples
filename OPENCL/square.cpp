#ifdef __APPLE__
    #include <OpenCL/cl.h>
#else
    #include <CL/cl.h>
#endif
#include <numeric>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include "common.h"
#include <cmath>


/*
 *
 *  In this example, we just run a basic kernel
 *
 *
 *
 *
 * */
 

cl_int clError = CL_SUCCESS;

cl_context context;
cl_program program;

cl_command_queue commandQueue;


const char * opencl_kernel_source() {

const char *kernelSource = "\n" \
"__kernel void square(                                                       \n" \
"   __global float* input,                                              \n" \
"   __global float* output)                                             \n" \
"{                                                                      \n" \
"   int i = get_global_id(0);                                           \n" \
"    output[i] = input[i] * input[i];                                \n" \
"}                                                                      \n" \
"\n";

    return kernelSource;


}


void opencl_clean(void) {
    clReleaseContext(context);
    clReleaseCommandQueue(commandQueue);
    clReleaseProgram(program);
    return;
}

void opencl_init() {


    // get the platform
    // ************************************************************************

    cl_uint num_platforms;
    clError = clGetPlatformIDs(0, NULL, &num_platforms);
    checkErr(clError, "clGetPlatformIDs( 0, NULL, &num_platforms );");

    if (num_platforms <= 0) {
        std::cout << "No platform..." << std::endl;
        exit(1);
    }

    cl_platform_id* platforms = new cl_platform_id[num_platforms];
    clError = clGetPlatformIDs(num_platforms, platforms, NULL);
    checkErr(clError, "clGetPlatformIDs( num_platforms, &platforms, NULL );");

    if (num_platforms > 1) {
        char platformName[256];
        clError = clGetPlatformInfo(platforms[0], CL_PLATFORM_VENDOR,
                sizeof(platformName), platformName, NULL);
        std::cerr << "Multiple platforms found defaulting to: " << platformName
                << std::endl;
    }

    cl_platform_id  platform_id = platforms[0];

    delete platforms;

    // get the devices (and queues...)
    // ************************************************************************

    cl_uint device_count = 0;
    clError = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ALL, 0, NULL,
            &device_count);
    checkErr(clError, "Failed to create a device group");
    cl_device_id* deviceIds = (cl_device_id*) malloc(
            sizeof(cl_device_id) * device_count);
    clError = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ALL, device_count,
            deviceIds, NULL);



    char device_name[256];
    int compute_units;

    clError = clGetDeviceInfo(deviceIds[0 % device_count], CL_DEVICE_NAME,
            sizeof(device_name), device_name, NULL);
    checkErr(clError, "clGetDeviceInfo failed");
    clError = clGetDeviceInfo(deviceIds[0 % device_count], CL_DEVICE_MAX_COMPUTE_UNITS,
            sizeof(cl_uint), &compute_units, NULL);
    checkErr(clError, "clGetDeviceInfo failed");
    std::cerr << "Device is : " << device_name;
    std::cerr << " with " << compute_units << " compute units" << std::endl;

    cl_device_id device_id = deviceIds[0];


    // Create a compute context
    //
    context = clCreateContext(0, device_count, deviceIds, NULL, NULL, &clError);
    checkErr(clError, "Failed to create a compute context!");

    delete deviceIds;

    // Create a command commands
    //
    commandQueue = clCreateCommandQueue(context, device_id, 0, &clError);
    checkErr(clError, "Failed to create a command commands!");

    const char* charSource = opencl_kernel_source();

    // Create the compute program from the source buffer
    //
    program = clCreateProgramWithSource(context, 1, (const char **) &charSource,
            NULL, &clError);
    if (!program) {
        std::cout << "Error: Failed to create compute program!" << std::endl;
        exit(1);
    }

    // Build the program executable
    //
    clError = clBuildProgram(program, 0, NULL, NULL, NULL,
            NULL);

    /* Get the size of the build log. */
    size_t logSize = 0;
    clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL,
            &logSize);

    if (clError != CL_SUCCESS) {
        if (logSize > 1) {
            char* log = new char[logSize];
            clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG,
                    logSize, log, NULL);

            std::string stringChars(log, logSize);
            std::cerr << "Build log:\n " << stringChars << std::endl;

            delete[] log;
        }
        std::cout <<  "Error: Failed to build program executable!" << std::endl;
        exit(1);
    }

    return;

}

////////////////////////////////////////////////////////////////////////////////
 
int job(const int DATA_SIZE)
{
      
    float data[DATA_SIZE];              // original data set given to device
    float results[DATA_SIZE];           // results returned from device
    unsigned int correct;               // number of correct results returned
 
    size_t global;                      // global domain size for our calculation
 
    // Create the compute kernel in the program we wish to run
    //
    static cl_kernel kernel = clCreateKernel(program, "square", &clError);
    if (!kernel || clError != CL_SUCCESS)
    {
        printf("Error: Failed to create compute kernel!\n");
        exit(1);
    }
 
    // Create the input and output arrays in device memory for our calculation
    //
    cl_mem input  = clCreateBuffer(context,  CL_MEM_READ_ONLY,  sizeof(float) * DATA_SIZE, NULL, NULL);
    cl_mem output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * DATA_SIZE, NULL, NULL);
    if (!input || !output)
    {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }    
    
    // Write our data set into the input array in device memory 
    //
    clError = clEnqueueWriteBuffer(commandQueue, input, CL_TRUE, 0, sizeof(float) * DATA_SIZE, data, 0, NULL, NULL);
    if (clError != CL_SUCCESS)
    {
        printf("Error: Failed to write to source array!\n");
        exit(1);
    }
 
    // Set the arguments to our compute kernel
    //
    clError = 0;
    clError = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input);
    clError|= clSetKernelArg(kernel, 1, sizeof(cl_mem), &output);
    if (clError != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", clError);
        exit(1);
    }
 
    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    //
    global = DATA_SIZE;
    clError = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, &global, NULL, 0, NULL, NULL);
    if (clError)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }
 
    // Wait for the command commands to get serviced before reading back results
    //
    clFinish(commandQueue);
 
    // Read back the results from the device to verify the output
    //
    clError = clEnqueueReadBuffer( commandQueue, output, CL_TRUE, 0, sizeof(float) * DATA_SIZE, results, 0, NULL, NULL );  
    if (clError != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", clError);
        exit(1);
    }
    
    // Validate our results
    //
    correct = 0;
    for(int i = 0; i < DATA_SIZE; i++)
    {
        if(results[i] == data[i] * data[i])
            correct++;
    }
    
    // Print a brief summary detailing the results
    //
    printf("Computed '%d/%d' correct values!\n", correct, DATA_SIZE);
    

 
    return 0;
}




int main(int argc, char** argv)
{

 uint datasize = 128;
 if (argc > 1) datasize = atoi(argv[1]);

  std::cout << "Initialize OpenCL." << std::endl;
  opencl_init();

 job(datasize);

  std::cout << "Clean OpenCL." << std::endl;
  opencl_clean();


}
