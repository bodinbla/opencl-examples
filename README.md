# OpenCL Examples #

This repository contains a few examples of my own (or not) to evaluate/test OpenCL devices.

It includes :

* [OPENCL](OPENCL) : A minimal set of OpenCL examples to test partitioning and shared memory. 
* [MARE](MARE) : A set of tests using the Qualcomm MARE SDK on heterogeneous devices.
* [PAPI](PAPI) : Just a minimal example for PAPI.